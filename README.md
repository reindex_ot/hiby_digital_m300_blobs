# HiBy Digital M300
HiBy Digital Android DAP.

# Specs
- OS: Android 13
- SoC: Qualcomm Snapdragon 665
- Storage: 32GB
- RAM: 3GB
- DAC: CS43131
- Wi-Fi: 2.4GHz/5GHz
- Display: 4.0 IPS Screen (1280×640)
- Battery: 2000mAh/3.8V